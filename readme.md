# Aggie Feed Wordpress widget

The Aggie Feed Wordpress widget plugin adds a widget that displays feed items from the UC Davis Aggie Feed service (https://aggiefeed.ucdavis.edu).

There are a number of options available to customize the output of the feed.

- **Title**: a title to be shown above the feed widget.
- **Feed source**: select all sources to show all feed content items, or choose one of the sources from the dropdown to limit feed content to that source.
- **Number of activities**: the number of feed items to show. Default is 5.
- **Limit post characters**: limit the number of characters displayed in the feed item content. Default is 512.
- **Activity icon type**: options are 'default','custom' or 'no'.  default: Show standard icons (notification = globe, event = calendar, poll = pencil), custom: Show activity source specific icon or image, no: Show no icons.
- **Filter by user roles**: Filter by user roles (student, faculty, staff).  This will only show posts for which the primary intended recipient was the person currently logged in. For example, if you are staff and a post is from the Internship and Career Center, then you will not see that post. User must be CAS authenticated via the host page.
- **Show basic post actions**: If present, it will show basic post actions such as content expand/collapse. It is also required for social and event sharing actions to show up.
- **Show social sharing icons**: Show social sharing icons for Facebook, Twitter and Google Plus. NOTE: requires 'Show basic post actions' to be enabled
- **Show event sharing icons**: Show event sharing icon, which contains: Add to Google Calendar and Download iCalendar File. NOTE: requires 'Show basic post actions' to be enabled
