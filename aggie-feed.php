<?php
/*
Plugin Name: AggieFeed widget
Plugin URI: https://bitbucket.org/simon_dvorak/aggie-feed-widget
Description: Displays a customizable Aggie Feed widget
Author: Simon Dvorak
Version: 1.0
Author URI: http://ats.ucavis.edu
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
wp_enqueue_style( 'embed-v2', 'http://aggiefeed.ucdavis.edu/css/embed-v2.min.css');
wp_enqueue_style( 'fontawesome4-3', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');

wp_enqueue_script( 'embed-v2', 'http://aggiefeed.ucdavis.edu/js/embed-v2.min.js', array('jquery'), '2.0.0', true );
wp_enqueue_script( 'aggie_feed_init', plugins_url() . '/aggie-feed/aggie_feed_init.js', array(), '0.1', true );
add_action( 'widgets_init', function(){
     register_widget( 'Aggie_feed' );
});	

class Aggie_feed extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Aggie_feed', // Base ID
			__('Aggie Feed', 'text_domain'), // Name
			array( 'description' => __( 'Aggie feed widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		//set defaults
		$data_num_activities = 'data-num-activities="5"';
		$data_content_limit = 'data-content-limit="512"';
		$data_activity_actions = '';
		$data_auth = '';
		$data_activity_icon = 'data-activity-icon ="default"';
		$data_activity_actions = '';
		$data_activity_action_social = '';
		$data_activity_action_event = '';
     	echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		if(! empty($instance['data-num-activities'])){
			$data_num_activities = "data-num-activities='{$instance['data-num-activities']}'";
		}
		if(! empty($instance['data-content-limit'])){
			$data_content_limit = "data-content-limit='{$instance['data-content-limit']}'";
		}
		if(! empty($instance['data-auth']) && $instance['data-auth'] == "1"){
			$data_auth = 'data-auth';
		}
		if(! empty($instance['data-activity-icon'])){
			$data_activity_icon = "data-activity-icon='{$instance['data-activity-icon']}'";
		}
		if(! empty($instance['data-activity-source'])){
			$data_activity_source = "data-activity-source='{$instance['data-activity-source']}'";
		}
		if(! empty($instance['data-activity-actions']) && $instance['data-activity-actions'] == '1'){
			$data_activity_actions = 'data-activity-actions';
		}
		if(! empty($instance['data-activity-action-social']) && $instance['data-activity-action-social'] == '1'){
			$data_activity_action_social = "data-activity-action-social";
			$data_activity_actions = 'data-activity-actions';
		}
		if(! empty($instance['data-activity-action-event']) && $instance['data-activity-action-event'] == '1'){
			$data_activity_action_event = "data-activity-action-event";
			$data_activity_actions = "data-activity-actions";
		}
		echo "<div data-aggiefeed {$data_num_activities} {$data_auth} {$data_activity_source} {$data_activity_actions} {$data_activity_action_social} {$data_activity_action_event} {$data_content_limit} {$data_activity_icon} class=\"aggiefeed-element\"></div>";
		echo $args['after_widget'];
		
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = __( 'New title', 'text_domain' );
		$data_num_activities = '5';
		$data_content_limit = '512';
		$data_activity_actions = '';
		$data_auth = '';
		$data_activity_icon = 'default';
		$data_activity_actions = '';
		$data_activity_action_social = '';
		$data_activity_action_event = '';
		$data_activity_source = '';
		
		$sources_url = "https://aggiefeed.ucdavis.edu/api/v1/source/public";
		$json = file_get_contents($sources_url);
		$sources = json_decode($json,true);

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		if ( isset($instance['data-num-activities'])){
			$data_num_activities = $instance['data-num-activities'];
		}
		if ( isset($instance['data-content-limit'])){
			$data_content_limit = $instance['data-content-limit'];
		}
		if ( isset($instance['data-auth']) && $instance['data-auth'] == "1"){
			$data_auth = 1;
		}
		if ( isset($instance['data-activity-icon'])){
			$data_activity_icon = $instance['data-activity-icon'];
		}
		if ( isset($instance['data-activity-actions']) && $instance['data-activity-actions'] == "1"){
			$data_activity_actions = 1;
		}
		if ( isset($instance['data-activity-action-social']) && $instance['data-activity-action-social'] == "1"){
			$data_activity_action_social = 1;
			$data_activity_actions = 1;
		}
		if ( isset($instance['data-activity-action-event']) && $instance['data-activity-action-event'] == "1"){
			$data_activity_action_event = 1;
			$data_activity_actions = 1;
		}
		if ( isset($instance['data-activity-source'])){
			$data_activity_source = $instance['data-activity-source'];
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'data-activity-source' ); ?>"><?php _e( 'Feed source:' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'data-activity-source' ); ?>" name="<?php echo $this->get_field_name( 'data-activity-source' ); ?>">
				<option value="">All sources</option>
				<?php 
					foreach($sources as $source){
						if($data_activity_source == $source['sourceId']){
							$selected = 'selected="selected"';
						}else{
							$selected = '';
						}
						echo "<option value='{$source['sourceId']}' {$selected}>{$source['sourceName']}</option>";
					}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'data-num-activities' ); ?>"><?php _e( 'Number of activities:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'data-num-activities' ); ?>" name="<?php echo $this->get_field_name( 'data-num-activities' ); ?>" type="text" value="<?php echo esc_attr( $data_num_activities ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'data-content-limit' ); ?>"><?php _e( 'Limit post characters:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'data-content-limit' ); ?>" name="<?php echo $this->get_field_name( 'data-content-limit' ); ?>" type="text" value="<?php echo esc_attr( $data_content_limit ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'data-activity-icon' ); ?>"><?php _e( 'Activity icon type:' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'data-activity-icon' ); ?>" name="<?php echo $this->get_field_name( 'data-activity-icon' ); ?>">
				<option value="no" <?php  if($data_activity_icon == "no"){echo 'selected="selected"';}; ?>>None</option>
				<option value="default" <?php  if($data_activity_icon == "default"){echo 'selected="selected"';}; ?>>Default</option>
				<option value="custom" <?php  if($data_activity_icon == "custom"){echo 'selected="selected"';}; ?>>Custom</option>
			</select>
		</p>
		<p>	
			<input id="<?php echo $this->get_field_id( 'data-auth' ); ?>" name="<?php echo $this->get_field_name( 'data-auth' ); ?>" type="checkbox" value="1" <?php  if($data_auth == "1"){echo 'checked="checked"';}; ?>>
			<label for="<?php echo $this->get_field_id( 'data-auth' ); ?>"><?php _e( 'Filter by user roles (staff,student,faculty)' ); ?></label>
		</p>
		<p>	
			<input id="<?php echo $this->get_field_id( 'data-activity-actions' ); ?>" name="<?php echo $this->get_field_name( 'data-activity-actions' ); ?>" type="checkbox" value="1" <?php  if($data_activity_actions == "1"){echo 'checked="checked"';}; ?>>
			<label for="<?php echo $this->get_field_id( 'data-activity-actions' ); ?>"><?php _e( 'Show basic post actions' ); ?></label>
		</p>
		<p>	
			<input id="<?php echo $this->get_field_id( 'data-activity-action-social' ); ?>" name="<?php echo $this->get_field_name( 'data-activity-action-social' ); ?>" type="checkbox" value="1" <?php  if($data_activity_action_social == "1"){echo 'checked="checked"';}; ?>>
			<label for="<?php echo $this->get_field_id( 'data-activity-action-social' ); ?>"><?php _e( 'Show social sharing icons' ); ?></label>
		</p>
		<p>	
			<input id="<?php echo $this->get_field_id( 'data-activity-action-event' ); ?>" name="<?php echo $this->get_field_name( 'data-activity-action-event' ); ?>" type="checkbox" value="1" <?php  if($data_activity_action_event== "1"){echo 'checked="checked"';}; ?>>
			<label for="<?php echo $this->get_field_id( 'data-activity-action-event' ); ?>"><?php _e( 'Show event sharing icons' ); ?></label>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['data-num-activities'] = ( ! empty( $new_instance['data-num-activities'] ) ) ? intval(strip_tags( $new_instance['data-num-activities'] )) : '';
		$instance['data-content-limit'] = ( ! empty( $new_instance['data-content-limit'] ) ) ? intval(strip_tags( $new_instance['data-content-limit'] )) : '';
		$instance['data-auth'] = ( ! empty( $new_instance['data-auth'] ) ) ? intval(strip_tags( $new_instance['data-auth'] )) : '';
		$instance['data-activity-source'] = ( ! empty( $new_instance['data-activity-source'] ) ) ? strip_tags( $new_instance['data-activity-source'] ) : '';
		$instance['data-activity-icon'] = ( ! empty( $new_instance['data-activity-icon'] ) ) ? strip_tags( $new_instance['data-activity-icon'] ) : '';
		$instance['data-activity-actions'] = ( ! empty( $new_instance['data-activity-actions'] ) ) ? intval(strip_tags( $new_instance['data-activity-actions'] )) : '';
		$instance['data-activity-action-social'] = ( ! empty( $new_instance['data-activity-action-social'] ) ) ? intval(strip_tags( $new_instance['data-activity-action-social'] )) : '';
		$instance['data-activity-action-event'] = ( ! empty( $new_instance['data-activity-action-event'] ) ) ? intval(strip_tags( $new_instance['data-activity-action-event'] )) : '';
		return $instance;
	}

} // class My_Widget